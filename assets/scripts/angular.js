/* globals angular, window */
var app = angular.module("hCardBuilder", []).config(function($compileProvider) {
    // disable debug info
    $compileProvider.debugInfoEnabled(false);
});

app.controller("formController",function() {
    var vm = this;
    vm.form = {
        sections: {
            personalDetails: {
                label: 'Personal details',
                fields: ['firstName', 'lastName', 'email', 'phone']
            },
            addressDetails: {
                label: 'Address',
                fields: ['streetNumber', 'streetName', 'suburb', 'state', 'postcode', 'country']
            }
        },
        fields: {
            firstName: {
                type: 'text',
                label: 'Given name'
            },
            lastName: {
                type: 'text',
                label: 'Surname'
            },
            email: {
                type: 'email',
                label: 'Email'
            },
            phone: {
                type: 'tel',
                label: 'Phone'
            },
            streetNumber: {
                type: 'text',
                label: 'House name or #'
            },
            streetName: {
                type: 'text',
                label: 'Street'
            },
            suburb: {
                type: 'text',
                label: 'Suburb'
            },
            state: {
                type: 'text',
                label: 'State'
            },
            postcode: {
                type: 'text',
                label: 'Postcode'
            },
            country: {
                type: 'text',
                label: 'Country'
            }

        }
    };

    vm.model = {};
    Object.keys(vm.form.fields).map(function(fieldId) {
        vm.model[fieldId] = '';
    });

    window.loadImage = function(img) {
        var isIE = (navigator.appName == "Microsoft Internet Explorer");
        var path = img.value;
        if (isIE) {
            document.getElementById('preview-image').setAttribute('src', path);
        } else {
            if (img.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    document.getElementById('preview-image').setAttribute('src', e.target.result);
                };
                reader.readAsDataURL(img.files[0]);
            }
        }
    };
});

angular.element(document).ready(function() {
    var template = document.getElementById("hCardTemplate");
    var appDiv = document.getElementById("root");
    setTimeout(function() {
        appDiv.innerHTML = template.innerHTML;
        angular.bootstrap(angular.element(appDiv), ['hCardBuilder']);
    }, 1000);
});