# Domain FE test (Vanilla JS and Angular.JS implementations)

## Prerequisites
- Node 5
- NPM 3

Clone repository to target folder and run ```npm install```.

To build project run ``npm run build``. After this you can use ``npm run server`` to start webserver. 

Navigate to [http://127.0.0.1:8080](http://127.0.0.1:8080) - vanilla version is located there. 

To load Angular.JS version navigate to [http://127.0.0.1:8080/index-angular.html](http://127.0.0.1:8080/index-angular.html).

The source code is located in ``assets`` folder.